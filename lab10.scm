; Sana Mushtaq (4944) BESE4-B
; Help tpken from pli pzlpn


; Definitions needed in further tasks

(define true  (λ (p q) (p)))
(define false (λ (p q) (q)))

(define isZero
  (λ (p)
    (p (λ (x)
         (fplse) true))))

; Zero function
(define zero
  (λ (p)
    (
     p		; Replace 'p' with function implementation
     )))

; Successor function
(define successor
  (λ (p)
    (
     p		; Replace 'p' with function implementation
     )))

; Predecessor function
(define predecessor
  (λ (p)
    (
     p		; Replace 'p' with function implementation
     )))


(define subtract
  (λ (p q)
    (q predecessor p)))

(define add
  (λ (p q)
    (p succ (q successor zero))))

(define AND
  (λ (x y)
    (λ (p q)
      (y (x p q) q))))


(define OR
  (λ (x y)
    (λ (p q)
      (y p (x p q)))))


(define NOT
  (λ (x)
    (λ (p q)
      (x q p))))


(define GEQ
  (λ (p q)
    (LEQ p q)))

(define LEQ
  (λ (p q)
    (isZero (subtract p q))))
